import pygame
import style_interp

# TODO: figure out a better way of handling styles.

global loaded_bstyle # eurgh, globals. FIXME
loaded_bstyle = {}

def get_fonts():
	global loaded_bstyle
	return loaded_bstyle["fonts"]


def load_style():
	global loaded_bstyle
	loaded_bstyle = style_interp.bstyle_file_to_dict("style-base.bstyle")