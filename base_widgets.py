import pygame
import fonts
import style

class base_widget():
	"""
		A base widget object. Ideally everything gui-related inherits from this.
	"""

	def __init__(self):
		"""
			Init the base widget.
			No arguments, you are intended to set them AFTER the fact.
		"""

		self.renderable: pygame.Surface = None # Our surface/renderable object
		self.surface: pygame.Surface = None
		self.position: pygame.Vector2 = pygame.Vector2(0,0)
		self.screen_position: pygame.Vector2 = pygame.Vector2(0,0)
		self.children: list = []
		self.parent = None
		self.style = style.loaded_bstyle.get("base-style", {})
		self.collision: pygame.Rect = None

		self.colour_bg = (0,0,0)
		self.colour_fg = (0,0,0)

		self.border = True
		self.border_colour = (0,0,0)
		self.border_thickness = 0

		self.font = None
		self.font_aa = False


	def add_child(self, child):
		child.surface = self.renderable
		child.screen_position = self.position + child.position # TODO: seems fragile
		child.parent = self
		self.children.append(child)

	def update(self):
		for child in self.children:
			child.update()


		# Update our styling while we're at it

		self.colour_bg = self.style.get("colour-bg", self.colour_bg)
		self.colour_fg = self.style.get("colour-fg", self.colour_fg)

		self.border = self.style.get("border-on", self.border)
		self.border_colour = self.style.get("border-colour", self.border_colour)
		self.border_thickness = int(self.style.get("border-thickness", self.border_thickness))

		self.font = fonts.get(self.style.get("font"))
		self.font_aa = self.style.get("font-aa", self.font_aa)

		# update our collision, position, etc.

		if self.parent:
			self.screen_position = self.parent.position + self.position
		else:
			self.screen_position = self.position

		if self.renderable:
			self.collision = self.renderable.get_rect()
			self.collision.topleft = self.screen_position

	def render_us(self):
		"""renders our widget"""

		self.renderable.fill(self.colour_bg)
		if self.border:
			pygame.draw.rect(surface=self.renderable, color=self.border_colour, rect=self.renderable.get_rect(), width=self.border_thickness)

	def render(self):

		if self.renderable:
			self.render_us()

		for child in self.children:
			child.render()

		self.surface.blit(self.renderable, self.position)
	
	def collide_point(self, point: pygame.Vector2) -> bool:
		return self.collision.collidepoint(point)
	
	def send_event(self, event: pygame.event.Event):
		for child in self.children: # The base widget doesn't do anything with this information, other than pass it down.
			child.send_event(event)

class base_control_widget(base_widget):
	"""
		This class provides callbacks for events.
	"""

	def on_clicked(self):
		pass
	def on_hold(self):
		pass
	def on_release(self):
		pass

	def send_event(self, event: pygame.event.Event):
		super().send_event(event)

		if event.type == pygame.MOUSEBUTTONDOWN:
			if self.collide_point(event.pos):
				self.on_hold()
		elif event.type == pygame.MOUSEBUTTONUP:
			if self.collide_point(event.pos):
				self.on_release()
				self.on_clicked()

class base_text_widget(base_widget):

	def __init__(self):
		super().__init__()

		self.font: pygame.font.Font = None
		self.text: str = None

	def render_us(self):

		super().render_us()

		self.renderable = self.font.render(self.text, self.font_aa, self.colour_fg)
