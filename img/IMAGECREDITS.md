# mandrill.png
![](mandrill.png)

USC-SIPI Image Database was the source I'd gotten it from.\
However, their image credit for it is simply a magazine scan, not listing what magazine.\
Much like the USC-SIPI, I give full copyrights to the original photographer/author of the Image, and would be more than happy to change it should any legal friction come about from my use of it.