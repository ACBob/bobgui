"""

	An interpreter for the bstyle spec.
	Turns it into a dictionary, which is easy to navigate.

"""

# debugging libraries are for losers
BSTYLE_DEBUG = False
def bstyle_debug_print(msg):
	if BSTYLE_DEBUG:
		print(msg)

def bstyle_file_to_dict(bstyle_file_path: str):
	file = open(bstyle_file_path)
	bstyle = file.read()
	file.close()

	bstyle_dict = bstyle_to_dict(bstyle)
	
	return bstyle_dict

# There may be use to a dict_to_bstyle, so just start the naming scheme
# This is the WHOLE parser here.
def bstyle_to_dict(bstyle: str) -> dict:

	# First we strip the comments.

	bstyle_raw_lines = bstyle.split("\n")
	bstyle_rules = []

	for line in bstyle_raw_lines:
		if not line or line[0] == "-":
			continue
		bstyle_rules.append(line)
	
	bstyle_debug_print("Cleared Comments resulting in;")
	bstyle_debug_print(bstyle_rules)

	# We now have a comment-less file.
	# What we do from here is we compress it into a dictionary of style groups.
	# Basically @style = { rules }

	current_group = None
	groups = {}
	included_files = []

	for line in bstyle_rules:
		if line[0] == "@":
			current_group = line[1:]
		elif line[0] == "\t":
			if current_group:
				if current_group in groups:
					groups[current_group] = [*groups[current_group], line[1:]]
				else:
					groups[current_group] = [line[1:]]
		elif line[0] == ":":
			key, value = line[1:].split("\t")
			if key == "include":
				included_files.append(value)
	
	bstyle_debug_print("unprocessed groups;")
	bstyle_debug_print(groups)

	# Now we split the keys into values for the groups.

	bstyle_groups = {}
	fonts = {}

	for group in groups:
		bstyle_groups[group] = {}
		for rule in groups[group]:
			key, value = rule.split('\t')
			if '\t' in group:
				gkey, gvalue = group.split('\t') # group key, group value

				# We try it against fonts first
				if gkey == "FONT":
					if not gvalue in fonts:
						fonts[gvalue] = {"names":[], "sizes":[], "file":None}

					if key == "size":
						fonts[gvalue]["names"].append("{}-{}".format(gvalue, value))
						fonts[gvalue]["sizes"].append(int(value))
					elif key == "file":
						fonts[gvalue]["file"] = value

					# It was a font, not a style group
					if group in bstyle_groups:
						del bstyle_groups[group]
					continue

			# Here we also do our pre-processing
			# Such as Colour Spelling
			
			# Keys first
			if "color" in key:
				key = key.replace("color", "colour")
			
			# Values last

			# Forcing it into a string
			if value[0] == "\"" and value[-1] == "\"":
				value = value[1:-1] # All we do here is strip the quote marks
			elif value[0] == "'" and value[-1] == "'":
				value = value[1:-1] # All we do here is strip the quote marks

			elif value[0] in ['-', '+']: # Numbers
				if value[1:].isdigit():
					value = float(value)
			
			elif value in ["1", "0"]: # Boolean
				value = value == "1"
			elif value in ["true", "false"]: # English Boolean, 'innit?
				value = value == "true"

			# colours
			elif value[0] == "#":
				hex_colour = value[1:]
				colour = []
				single = False # if the hex is a single or double

				# double
				if len(hex_colour) in [8, 6]:
					hex_colour = hex_colour[::2]
				# single
				if len(hex_colour) in [4, 3]:
					hex_colour = hex_colour[::1]
					single = True
				

				for portion in hex_colour:
					singlet = int(portion, 16)

					if single:
						colour.append(singlet*17)
					else:
						colour.append(singlet)
				
				value = tuple(colour)

			# this one should always be last
			# referring to a previous key
			elif value in bstyle_groups[group]:
				value = bstyle_groups[group][value]
			

			bstyle_groups[group][key] = value


	# Now we include files
	if included_files:
		bstyle_debug_print("Including files {}".format(included_files))

		for file in included_files:
			try:
				bstyle_to_merge = bstyle_file_to_dict(file)

				if "fonts" in bstyle_to_merge:
					font_rules = bstyle_to_merge["fonts"]
					our_font_rules = fonts

					fonts = font_rules.update(our_font_rules)

				bstyle_to_merge.update(bstyle_groups)
				bstyle_groups = bstyle_to_merge
			except FileNotFoundError:
				print("File {} couldn't be found. Ignoring...".format(file))

	if fonts:
		bstyle_groups["fonts"] = fonts

	bstyle_debug_print("the final dictionary;")
	bstyle_debug_print(bstyle_groups)

	return bstyle_groups


if __name__ == "__main__":
	print("Testing BStyle")
	BSTYLE_DEBUG = True
	print(bstyle_file_to_dict("style-base.bstyle"))