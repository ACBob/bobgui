import pygame

import style_interp
import basic_widgets
import style
import fonts

import sys



if __name__ == "__main__":
	print("Testing BobGUI")

	pygame.init()

	display = pygame.display.set_mode((800,600))

	# Load fonts first
	style.load_style()
	fonts.load_base_fonts()

	gui = basic_widgets.gui_root_widget((800,600), display)

	test_bit = basic_widgets.base_widget()
	test_bit.style = style.loaded_bstyle["style-base"]
	test_bit.renderable = pygame.Surface((300,200))

	test_label = basic_widgets.label_widget("Hello, BobGUI!", pygame.Vector2(16,8))
	test_label.style = style.loaded_bstyle["style-base"]

	test_label.on_clicked = lambda: print("Clicked Label")

	test_bit.add_child(test_label)
	gui.add_child(test_bit)

	run = True
	while run:
		display.fill((50,50,50))

		gui.update()
		gui.render()

		for event in pygame.event.get():
			gui.send_event(event)

			if event.type == pygame.QUIT:
				run = False
				break

		pygame.display.flip()

