import pygame
import style

global loaded_fonts, base_font # Eurgh, globals. FIXME
loaded_fonts = {} # Fonts


def load_base_fonts():
	global loaded_fonts, base_font

	if not pygame.font.get_init():
		pygame.font.init()

	base_font = pygame.font.Font("fonts/ACBobz.ttf", 8)
	fonts = style.get_fonts()

	for font in fonts:
		for size in range(len(fonts[font]["sizes"])):
			loaded_fonts[fonts[font]["names"][size]] = pygame.font.Font(fonts[font]["file"], fonts[font]["sizes"][size])

def get(name):
	return loaded_fonts.get(name, base_font)
	