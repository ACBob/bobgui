import pygame
from base_widgets import *

class label_widget(base_text_widget, base_control_widget):

	def __init__(self, text, position):
		super().__init__()

		self.text = text
		self.position = position
	
	def update(self):
		super().update()

		self.renderable = pygame.Surface(self.font.size(self.text), pygame.SRCALPHA, 32)


class gui_root_widget(base_widget):
	"""Provides a root for widgets to sit on."""
	
	def __init__(self,size, display):
		super().__init__()

		self.renderable = pygame.Surface(size, pygame.SRCALPHA, 32)
		self.surface = display

	def render_us(self):
		return # we don't have our own appearance